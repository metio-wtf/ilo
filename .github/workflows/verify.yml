name: Verify
on:
  pull_request:
    branches: [ master ]
  push:
    branches: [ master ]
    paths:
      - "src/**"
env:
  JAVA: "java11"
  GRAAL: "20.2.0"
jobs:
  linux:
    name: Linux packages
    runs-on: ubuntu-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v2
      - id: graal
        name: GraalVM Setup
        uses: rinx/setup-graalvm-ce@v0.0.5
        with:
          graalvm-version: ${{ env.GRAAL }}
          java-version: ${{ env.JAVA }}
          native-image: "true"
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v1
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: gpg
        name: GPG Key
        run: echo "${{ secrets.GPG_KEY }}" | base64 --decode > signing.key.asc
      - id: verify
        name: Verify Project
        run: mvn --batch-mode --settings $GITHUB_WORKSPACE/build/settings.xml verify -Dskip.graal=false
        env:
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
          GPG_KEY_PASSPHRASE: ${{ secrets.GPG_KEY_PASSPHRASE }}
      - name: Upload Java 11 Artifact
        uses: actions/upload-artifact@v2
        with:
          name: java11
          path: ./target/ilo-*-java11*
      - name: Upload Linux Artifact
        uses: actions/upload-artifact@v2
        with:
          name: linux
          path: ./target/ilo-*-linux*
  mac:
    name: MacOS packages
    runs-on: macos-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v2
      - id: graal
        name: GraalVM Setup
        uses: DeLaGuardo/setup-graalvm@master
        with:
          graalvm-version: "${{ env.GRAAL }}.${{ env.JAVA }}"
      - id: native-image
        name: Install native-image
        run: gu install native-image
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v1
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: gpg
        name: GPG Key
        run: echo "${{ secrets.GPG_KEY }}" | base64 --decode > signing.key.asc
      - id: verify
        name: Verify Project
        run: mvn --batch-mode --settings $GITHUB_WORKSPACE/build/settings.xml verify -Dskip.graal=false
        env:
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
          GPG_KEY_PASSPHRASE: ${{ secrets.GPG_KEY_PASSPHRASE }}
      - name: Upload Mac Artifact
        uses: actions/upload-artifact@v2
        with:
          name: mac
          path: ./target/ilo-*-mac*
  windows:
    name: Windows packages
    runs-on: windows-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v2
      - id: graal-download
        name: GraalVM Download
        run: Invoke-RestMethod -Uri https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-${{ env.GRAAL }}/graalvm-ce-${{ env.JAVA }}-windows-amd64-${{ env.GRAAL }}.zip -OutFile 'graal.zip'
      - id: graal-expand
        name: Install GraalVM
        run: Expand-Archive -Path 'graal.zip' -DestinationPath '.'
      - id: native-image
        name: Install native-image
        run: graalvm-ce-${{ env.JAVA }}-${{ env.GRAAL }}\bin\gu.cmd install native-image
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v1
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: gpg
        name: GPG Key
        uses: timheuer/base64-to-file@v1.0.3
        with:
          fileName: signing.key.asc
          encodedString: ${{ secrets.GPG_KEY }}
      - id: copy-key
        name: Copy GPG key to correct location
        run: Copy-Item -Path "${{ steps.gpg.outputs.filePath }}" -Destination "$env:GITHUB_WORKSPACE\signing.key.asc"
      - id: verify
        name: Verify Project
        shell: cmd
        run: |
          call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat" x86_amd64
          mvn --batch-mode --settings .\build\settings.xml verify -Dskip.graal=false -Dpit.skip=true
        env:
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
          GPG_KEY_PASSPHRASE: ${{ secrets.GPG_KEY_PASSPHRASE }}
          JAVA_HOME: ./graalvm-ce-${{ env.JAVA }}-${{ env.GRAAL }}
      - name: Upload Windows Artifact
        uses: actions/upload-artifact@v2
        with:
          name: windows
          path: ./target/ilo-*-windows*